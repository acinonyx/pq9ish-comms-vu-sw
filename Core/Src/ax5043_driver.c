/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ax5043_driver.h"
#include <stm32l4xx_hal.h>
#include "main.h"
#include "utils.h"
#include "radio.h"
#include <Drivers/AX5043/include/ax5043.h>
#include <string.h>


#define SPI_TIMEOUT      1000
extern SPI_HandleTypeDef hspi2;

/**
 * SPI chip select pin level
 * @param cs set 1 to set the CS signal high, 0 to set it low
 * @return 0 on success or negative error code
 */
int
ax5043_spi_cs(uint8_t cs)
{
	HAL_GPIO_WritePin(AX5043_SEL_GPIO_Port, AX5043_SEL_Pin, cs & 0x1);
	return AX5043_OK;
}

/**
 * Writes and reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
int
ax5043_spi_trx(uint8_t *rx, const uint8_t *tx, uint32_t len)
{
	return HAL_SPI_TransmitReceive(&hspi2, tx, rx, len, SPI_TIMEOUT);;
}

/**
 * Reads data from the IC using the SPI
 * @param rx buffer to store the received data
 * @param len the number of bytes to be sent and received
 * @return 0 on success or negative error code
 */
int
ax5043_spi_rx(uint8_t *rx, uint32_t len)
{
	return HAL_SPI_Receive(&hspi2, rx, len, SPI_TIMEOUT);
}

/**
 * Write data to the IC using the SPI
 * @param tx buffer containing the data to transmit
 * @param len the number of bytes to transmit
 * @return 0 on success or negative error code
 */
int
ax5043_spi_tx(const uint8_t *tx, uint32_t len)
{
	return HAL_SPI_Transmit(&hspi2, tx, len, SPI_TIMEOUT);
}

/**
 * Delay the execution with microsecond accuracy
 * @param us the delay in microseconds
 */
void
ax5043_usleep(uint32_t us)
{
	usleep(us);
}

/**
 * Returns the time in milliseconds. The counter should be monotonically increasing.
 * Internally the driver handles the event of a wrap around.
 * @return the time in milliseconds
 */
uint32_t
ax5043_millis()
{
	return millis();
}

void
ax5043_irq_enable(bool enable)
{
	if (enable) {
		NVIC_EnableIRQ(EXTI9_5_IRQn);
	} else {
		NVIC_DisableIRQ(EXTI9_5_IRQn);
	}
}

/**
 * Before any transmission, the driver calls this user defined function.
 * This can be used to select properly a signal path by activating a corresponding
 * RF switch and/or activate an external PA
 */
void
ax5043_tx_pre_callback()
{
}

/**
 * This function is automatically called by the driver at the end of every
 * transmission. In case of any error during a transmission session, the
 * driver ensures that this callback is called. Therefore it can be safely
 * used to disable any external active components like RF switches and/or
 * PA.
 *
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 */
void
ax5043_tx_post_callback()
{
	radio_tx_complete();
}

/**
 * Callback that is called when a frame is received completely.
 * @note This function is called inside the ISR so should be ISR friendly,
 * meaning that should not block and do as fast as possible its task
 *
 * @param pdu pointer to the received frame
 * @param len the number of byte received
 */
void
ax5043_rx_complete_callback(const uint8_t *pdu, size_t len)
{
	radio_frame_received(pdu, len);
}
