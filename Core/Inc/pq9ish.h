/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PQ9ISH_H_
#define PQ9ISH_H_

#include "ft_storage.h"
#include "radio.h"
#include "conf.h"
#include "fsm.h"

/**
 * @brief struct to store previous reasons of reset
 * Includes counters for each individual candidate reason
 */
struct ror_counters {
	// Counter of resets caused by low power
	uint16_t low_power_counter;
	// Counter of resets caused by independent watchdog
	uint16_t independent_watchdog_counter;
	// Counter of resets caused by software
	uint16_t software_counter;
	// Counter of resets caused by brownout
	uint16_t brownout_counter;
};

/**
 * @brief settings stored in flash
 * Size of this struct should not exceed 223 bytes
 */
struct pq9ish_settings {
	uint32_t init;
	uint32_t sat_id;
	struct ror_counters reset_counters;
	uint32_t tx_freq;
	uint32_t rx_freq;
	int8_t tm_resp_tx_power;
	radio_encoding_t tm_resp_enc;
	radio_modulation_t tm_resp_mod;
	radio_baud_t tm_resp_baud;
	uint8_t tm_resp_pa_en;
	uint8_t sha256[24];
	uint8_t mute_flag;
};

/**
 * @brief Hardware status bits
 */
struct pq9ish_hw_status {
	uint8_t power_save;		//!< Power save mode 1: True
	uint8_t power_mon_fail;	//!< Power monitor failed 1: True
};

struct pq9ish {
	struct radio hradio;
	struct ft_storage ft_persist_mem;
	struct pq9ish_settings settings;
	fsm_state_t fsm_state;
	uint32_t uptime_secs;
	struct pq9ish_hw_status status;
	uint8_t reason_of_reset;
};

int
pq9ish_init(struct pq9ish *q);

int
pq9ish_read_settings(struct pq9ish *q);

int
pq9ish_write_settings(struct pq9ish *q);

#endif /* PQ9ISH_H_ */
