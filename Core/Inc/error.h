/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H_
#define ERROR_H_

/**
 * PQ9ISH COMMS error codes
 */
enum {
	NO_ERROR,   //!< All ok
	INVAL_PARAM,//!< Invalid parameter
	OSDLP_INIT_ERROR, //!< OSDLP initialization error
	OSDLP_MTX_LOCK, //!< OSDLP mutex not locked
	OSDLP_QUEUE_EMPTY, //!< Access to empty queue
	OSDLP_QUEUE_FULL, //!< Access to full queue
	OSDLP_QUEUE_MEM_ERROR, //!< Queue memory error
	OSDLP_NULL, //!< Access to NULL structure
	FT_STORAGE_INIT_ERROR,  //!< Fault tolerant storage have not properly initialized
	FT_STORAGE_COR_MEM, //!< Flash storage is corrupted and contains invalid data
	FLASH_ERASE_ERROR, //!< Flash erase error
	FLASH_PROGRAM_ERROR, //!< Flash program error
	MTX_TIMEOUT_ERROR, //!< Mutex timeout error
	PQ9ISH_NOT_IMPL, //!< Not enough time baby :(
	PQ9ISH_PWR_ERROR, //!< Power reading error
	PQ9ISH_BUFFER_OVF, //!< Exceeded a buffer size
	PQ9ISH_ERROR_NUM //!< Total number of errors. Should be always last!
} pq9ish_error_t;

static int
HAL_to_pq9ish_error(int err)
{
	return err + PQ9ISH_ERROR_NUM;
}

#endif /* ERROR_H_ */
